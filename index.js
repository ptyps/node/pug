"use strict"

const pug = require('pug')

module.exports.render = (buffer, opts = {}) => new Promise((res, rej) => {
  pug.render(buffer, opts, (err, html) => {
    err ? rej(err) : res(html)
  })
})

module.exports.renderFile = (file, opts = {}) => new Promise((res, rej) => {
  pug.renderFile(file, opts, (err, html) => {
    err ? rej(err) : res(html)
  })
})